
# Summary

Common code for the backend services
accessible throug a nuget package, published on the private feed


# Publish locally
Based on the following [github issue](https://github.com/NuGet/Home/issues/7792#issuecomment-463399986)

Install new credential provider:
https://github.com/Microsoft/artifacts-credprovider

(the provided key doesn't matter)
'
dotnet nuget push --source "energix-nuget" --api-key "az" "energix.common.backend\bin\Debug\energix-common-backend.1.0.0.nupkg"
'

# Publish from buildserver

Uses the following 'build time added' config with repository variable to add authorization to the private feed

</configuration>
	<packageSourceCredentials>
		<energix-nuget>
			<add key="Username" value="PAT" /><add key="ClearTextPassword" value="$PAT" />
		</energix-nuget>
	</packageSourceCredentials>
</configuration>



If buildserver was windows , you could do something like 
- nuget.exe setapikey YOUR_PAT -source https://example.com/nugetfeed -c .\solution\nuget.config.
  There is no cross platform equivalent yet: https://github.com/NuGet/Home/issues/6437



