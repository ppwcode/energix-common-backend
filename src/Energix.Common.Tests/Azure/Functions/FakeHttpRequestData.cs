﻿using System.Security.Claims;

using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;

namespace Energix.Common.Tests.Azure.Functions
{
    public class FakeHttpRequestData : HttpRequestData
    {
        private readonly Stream _body;
        private readonly IReadOnlyCollection<IHttpCookie> _cookies;
        private readonly FunctionContext _functionContext;
        private readonly HttpHeadersCollection _headers;
        private readonly IEnumerable<ClaimsIdentity> _identities;
        private readonly string _method;
        private readonly string _url;

        public FakeHttpRequestData(
            FunctionContext functionContext,
            string method,
            string uri)
            : this(
                functionContext,
                method,
                uri,
                null,
                null,
                null,
                null)
        {
        }

        public FakeHttpRequestData(
            FunctionContext functionContext,
            string method,
            string url,
            Stream? body,
            HttpHeadersCollection? headers,
            IReadOnlyCollection<IHttpCookie>? cookies,
            IEnumerable<ClaimsIdentity>? identities)
            : base(functionContext)
        {
            _functionContext = functionContext;
            _method = method;
            _url = url;
            _body = body ?? Stream.Null;
            _headers = headers ?? new HttpHeadersCollection();
            _cookies = cookies ?? Array.Empty<IHttpCookie>();
            _identities = identities ?? Enumerable.Empty<ClaimsIdentity>();
        }

        public override Stream Body
            => _body;

        public override HttpHeadersCollection Headers
            => _headers;

        public override IReadOnlyCollection<IHttpCookie> Cookies
            => _cookies;

        public override Uri Url
            => new Uri(_url);

        public override IEnumerable<ClaimsIdentity> Identities
            => _identities;

        public override string Method
            => _method;

        public override HttpResponseData CreateResponse()
            => new FakeHttpResponseData(_functionContext);
    }
}
