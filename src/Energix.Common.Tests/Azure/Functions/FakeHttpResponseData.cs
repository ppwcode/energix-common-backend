﻿using System.Net;

using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;

namespace Energix.Common.Tests.Azure.Functions
{
    public class FakeHttpResponseData : HttpResponseData
    {
        private readonly HttpCookies _cookies;
        private Stream _body;
        private HttpHeadersCollection _headers;
        private HttpStatusCode _statusCode;

        public FakeHttpResponseData(
            FunctionContext functionContext,
            HttpStatusCode statusCode = HttpStatusCode.OK)
            : this(
                functionContext,
                statusCode,
                null,
                null,
                null)
        {
        }

        public FakeHttpResponseData(
            FunctionContext functionContext,
            HttpStatusCode statusCode,
            Stream? body,
            HttpHeadersCollection? headers,
            HttpCookies? cookies)
            : base(functionContext)
        {
            _statusCode = statusCode;
            _body = body ?? Stream.Null;
            _headers = headers ?? new HttpHeadersCollection();
            _cookies = cookies ?? new FakeCookies();
        }

        public override HttpStatusCode StatusCode
        {
            get => _statusCode;
            set => _statusCode = value;
        }

        public override HttpHeadersCollection Headers
        {
            get => _headers;
            set => _headers = value;
        }

        public override Stream Body
        {
            get => _body;
            set => _body = value;
        }

        public override HttpCookies Cookies
            => _cookies;

        private class FakeCookies : HttpCookies
        {
            private readonly IList<IHttpCookie> _cookies;

            public FakeCookies()
                : this(null)
            {
            }

            private FakeCookies(IList<IHttpCookie>? cookies)
            {
                _cookies = cookies ?? new List<IHttpCookie>();
            }

            public override void Append(string name, string value)
            {
                Append(new HttpCookie(name, value));
            }

            public override void Append(IHttpCookie cookie)
            {
                _cookies.Add(cookie);
            }

            public override IHttpCookie CreateNew()
                => new HttpCookie(string.Empty, string.Empty);
        }
    }
}
