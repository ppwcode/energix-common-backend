﻿using Microsoft.Azure.Functions.Worker;

using Moq;

namespace Energix.Common.Tests.Azure.Functions
{
    public abstract class BaseFunctionTest
    {
        private readonly Mock<FunctionContext> _functionContext;

        protected BaseFunctionTest()
        {
            _functionContext = new Mock<FunctionContext>();
        }

        protected FunctionContext FunctionContext
            => _functionContext.Object;
    }
}
